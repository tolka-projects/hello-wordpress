import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import Img from "gatsby-image"

export default function  IndexPage ({ data }) {
  console.log(data);
  return(
    <Layout>
      <SEO title="Home" />
      <h1>Hi people</h1>
      {data.allWordpressPost.edges.map(({ node }) => (
          <div>
            <p>{node.title}</p>
            <div dangerouslySetInnerHTML={{ __html: node.excerpt }} />
            <Link to={node.slug}>
              <p>{node.title}</p>
            </Link>
            <Img resolutions={node.childWordPressAcfPostPhoto.photo.localFile.childImageSharp.resolutions} 
            key={node.childWordPressAcfPostPhoto.photo.localFile.childImageSharp.resolutions.src} />
          </div>
        ))}
      <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
       
      </div>
      <Link to="/page-2/">Go to page 2</Link> <br />
      <Link to="/using-typescript/">Go to "Using TypeScript"</Link>
    </Layout>
  )
}




export const pageQuery = graphql`
  query {
    allWordpressPost(sort: { fields: [date] }) {
      edges {
        node {
          title
          excerpt
          slug
          childWordPressAcfPostPhoto {
            photo {
              localFile {
                childImageSharp {
                  resolutions(width: 500, height: 500) {
                    ...GatsbyImageSharpResolutions_withWebp_tracedSVG
                  }
                }
              }
            }
          }
        }
      }
    }
    
  }
`
